#!/bin/sh

die() {
	echo "$@" 1>&2
	exit 1
}

try() {
	"$@" || die "failed: $*"
}

try yarn prettier --write .
try yarn build
try mage -v
