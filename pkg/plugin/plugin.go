package plugin

import (
	"bytes"
	"context"
	"encoding/json"
	"time"
	"net/http"
	"net/url"
	"path"
	"errors"

	"github.com/grafana/grafana-plugin-sdk-go/backend"
	"github.com/grafana/grafana-plugin-sdk-go/backend/instancemgmt"
	//"github.com/grafana/grafana-plugin-sdk-go/backend/log"
	"github.com/grafana/grafana-plugin-sdk-go/data"
)

// Make sure SampleDatasource implements required interfaces. This is important to do
// since otherwise we will only get a not implemented error response from plugin in
// runtime. In this example datasource instance implements backend.QueryDataHandler,
// backend.CheckHealthHandler, backend.StreamHandler interfaces. Plugin should not
// implement all these interfaces - only those which are required for a particular task.
// For example if plugin does not need streaming functionality then you are free to remove
// methods that implement backend.StreamHandler. Implementing instancemgmt.InstanceDisposer
// is useful to clean up resources used by previous datasource instance when a new datasource
// instance created upon datasource settings changed.
var (
	_ backend.QueryDataHandler      = (*SampleDatasource)(nil)
	_ backend.CheckHealthHandler    = (*SampleDatasource)(nil)
	_ instancemgmt.InstanceDisposer = (*SampleDatasource)(nil)
)

// NewSampleDatasource creates a new datasource instance.
func NewSampleDatasource(_ backend.DataSourceInstanceSettings) (instancemgmt.Instance, error) {
	return &SampleDatasource{}, nil
}

// SampleDatasource is an example datasource which can respond to data queries, reports
// its health and has streaming skills.
type SampleDatasource struct{}

// Dispose here tells plugin SDK that plugin wants to clean up resources when a new instance
// created. As soon as datasource settings change detected by SDK old datasource instance will
// be disposed and a new one will be created using NewSampleDatasource factory function.
func (d *SampleDatasource) Dispose() {
	// Clean up datasource instance resources.
}

// QueryData handles multiple queries and returns multiple responses.
// req contains the queries []DataQuery (where each query contains RefID as a unique identifier).
// The QueryDataResponse contains a map of RefID to the response for each query, and each response
// contains Frames ([]*Frame).
func (d *SampleDatasource) QueryData(ctx context.Context, req *backend.QueryDataRequest) (*backend.QueryDataResponse, error) {
	// create response struct
	response := backend.NewQueryDataResponse()

	// loop over queries and execute them individually.
	for _, q := range req.Queries {
		res := d.query(ctx, req.PluginContext, q)

		// save the response in a hashmap
		// based on with RefID as identifier
		response.Responses[q.RefID] = res
	}

	return response, nil
}

type queryModel struct {
	QueryText string `json:"queryText"`
	Hidden bool `json:"hide"`
}

type tqlQuery struct {
	Version int32 `json:"version"`
	QueryText string `json:"query-text"`
	SampleInterval int64 `json:"sample-interval"`
	WindowStart int64 `json:"window-start"`
	WindowStop int64 `json:"window-stop"`
}

type tqlDataPoint struct {
	Time int64 `json:"time"`
	Value float64 `json:"value"`
}

type tqlSeries struct {
	Label string `json:"label"`
	DataPoints []tqlDataPoint `json:"data-points"`
}

type tqlData struct {
	Series []tqlSeries `json:"series"`
}

type tqlError struct {
	Code string `json:"code"`
	Message string `json:"message"`
}

type tqlSettings struct {
	Url string `json:"url"`
}

func (d *SampleDatasource) query(_ context.Context, pCtx backend.PluginContext, query backend.DataQuery) backend.DataResponse {
	response := backend.DataResponse{}

	// Decode the settings.
	settings := &tqlSettings{}
	err := json.Unmarshal(pCtx.DataSourceInstanceSettings.JSONData, &settings)

	if err != nil {
		response.Error = err
		return response
	}

	url, err := url.Parse(settings.Url)

	if err != nil {
		response.Error = err
		return response
	}

	url.Path = path.Join(url.Path, "query")

	// Unmarshal the JSON into our queryModel.
	qm := &queryModel{}
	err = json.Unmarshal(query.JSON, &qm)

	if err != nil {
		response.Error = err
		return response
	}

	if qm.Hidden {
		return response
	}

	var tq tqlQuery
	tq.Version = 1
	tq.QueryText = qm.QueryText
	tq.SampleInterval = query.Interval.Milliseconds()
	tq.WindowStart = query.TimeRange.From.UnixMilli()
	tq.WindowStop = query.TimeRange.To.UnixMilli()

	buffer := new(bytes.Buffer)
	json.NewEncoder(buffer).Encode(tq)
	req, _ := http.NewRequest("POST", url.String(), buffer)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: time.Second * 30}
	resp, err := client.Do(req)

	if err != nil {
		response.Error = err
		return response
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		tqe := &tqlError{}
		err = json.NewDecoder(resp.Body).Decode(tqe)

		if err != nil {
			response.Error = err
			return response
		}

		response.Error = errors.New(tqe.Message)
		return response
	}

	tqd := &tqlData{}

	err = json.NewDecoder(resp.Body).Decode(tqd)

	if err != nil {
		response.Error = err
		return response
	}

	defer resp.Body.Close()

	for _, series := range tqd.Series {
		frame := data.NewFrame(series.Label)
		frame.RefID = query.RefID

		config := &data.FieldConfig{}
		config.DisplayNameFromDS = series.Label

		frame.Fields = append(frame.Fields,
			data.NewField("time", nil, make([]time.Time, len(series.DataPoints))),
			data.NewField("values", nil, make([]float64, len(series.DataPoints))),
		)

		frame.Fields[1].Config = config

		for j, dataPoint := range series.DataPoints {
			frame.Set(0, j, time.UnixMilli(dataPoint.Time))
			frame.Set(1, j, dataPoint.Value)
		}

		response.Frames = append(response.Frames, frame)
	}

	return response
}

// CheckHealth handles health checks sent from Grafana to the plugin.
// The main use case for these health checks is the test button on the
// datasource configuration page which allows users to verify that
// a datasource is working as expected.
func (d *SampleDatasource) CheckHealth(_ context.Context, healthReq *backend.CheckHealthRequest) (*backend.CheckHealthResult, error) {
	// Decode the settings.
	settings := &tqlSettings{}
	err := json.Unmarshal(healthReq.PluginContext.DataSourceInstanceSettings.JSONData, &settings)

	if err != nil {
		return &backend.CheckHealthResult{
			Status:  backend.HealthStatusError,
			Message: err.Error(),
		}, nil
	}

	url, err := url.Parse(settings.Url)

	if err != nil {
		return &backend.CheckHealthResult{
			Status:  backend.HealthStatusError,
			Message: err.Error(),
		}, nil
	}

	url.Path = path.Join(url.Path, "query")

	stop := time.Now().UnixMilli()

	var tq tqlQuery
	tq.Version = 1
	tq.QueryText = "1"
	tq.SampleInterval = 100
	tq.WindowStart = stop - 1000
	tq.WindowStop = stop

	buffer := new(bytes.Buffer)
	json.NewEncoder(buffer).Encode(tq)
	req, _ := http.NewRequest("POST", url.String(), buffer)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: time.Second * 10}
	resp, err := client.Do(req)

	if err != nil {
		return &backend.CheckHealthResult{
			Status:  backend.HealthStatusError,
			Message: err.Error(),
		}, nil
	}

	if resp.StatusCode < 200 || resp.StatusCode >= 300 {
		tqe := &tqlError{}
		err = json.NewDecoder(resp.Body).Decode(tqe)

		if err != nil {
			return &backend.CheckHealthResult{
				Status:  backend.HealthStatusError,
				Message: err.Error(),
			}, nil
		}

		return &backend.CheckHealthResult{
			Status:  backend.HealthStatusError,
			Message: tqe.Message,
		}, nil
	}

	tqd := &tqlData{}
	err = json.NewDecoder(resp.Body).Decode(tqd)

	if err != nil {
		return &backend.CheckHealthResult{
			Status:  backend.HealthStatusError,
			Message: err.Error(),
		}, nil
	}

	return &backend.CheckHealthResult{
		Status:  backend.HealthStatusOk,
		Message: "Datasource test successful.",
	}, nil
}

