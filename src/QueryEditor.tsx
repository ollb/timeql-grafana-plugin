import { defaults } from 'lodash';

import React, { PureComponent } from 'react';
import { CodeEditor } from '@grafana/ui';
import { QueryEditorProps } from '@grafana/data';
import { DataSource } from './datasource';
import { defaultQuery, TqlDataSourceOptions, TqlQuery } from './types';

type Props = QueryEditorProps<DataSource, TqlQuery, TqlDataSourceOptions>;

export class QueryEditor extends PureComponent<Props> {
  onQueryTextChange = (queryText: string) => {
    const { onChange, query } = this.props;
    onChange({ ...query, queryText: queryText });
  };

  render() {
    const query = defaults(this.props.query, defaultQuery);
    const { queryText } = query;

    return (
      <>
        <CodeEditor
          value={queryText || ''}
          onSave={this.onQueryTextChange}
          onBlur={this.onQueryTextChange}
          height={'400px'}
          language="clojure"
          showMiniMap={false}
          showLineNumbers={true}
          // Requires Monaco 0.32
          //monacoOptions={{ bracketPairColorization: { enabled: true }}}
        />
      </>
    );
  }
}
