import { defaults } from 'lodash';
import { DataSourceInstanceSettings, DataQueryRequest } from '@grafana/data';
import { DataSourceWithBackend, getTemplateSrv } from '@grafana/runtime';
import { TqlDataSourceOptions, TqlQuery, defaultQuery } from './types';

export class DataSource extends DataSourceWithBackend<TqlQuery, TqlDataSourceOptions> {
  constructor(instanceSettings: DataSourceInstanceSettings<TqlDataSourceOptions>) {
    super(instanceSettings);
  }

  applyTemplateVariables(query: TqlQuery) {
    const templateSrv = getTemplateSrv();
    return {
      ...query,
      queryText: query.queryText ? templateSrv.replace(query.queryText) : '',
    };
  }

  query(request: DataQueryRequest<TqlQuery>) {
    return super.query({
      ...request,
      targets: request.targets.map((target) => {
        return defaults(target, defaultQuery);
      }),
    });
  }
}
