import { DataQuery, DataSourceJsonData } from '@grafana/data';

export interface TqlQuery extends DataQuery {
  queryText: string;
}

export const defaultQuery: Partial<TqlQuery> = {
  queryText: '(gen-series #(* % %))',
};

/**
 * These are options configured for each DataSource instance.
 */
export interface TqlDataSourceOptions extends DataSourceJsonData {
  url?: string;
}
