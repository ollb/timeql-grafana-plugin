# TimeQL Grafana Plugin

This plugin for [Grafana](https://grafana.com) enables the use of [TimeQL](https://gitlab.com/ollb/timeql)
as a data source.

## Installation

You can download binary packages from the [package registry](https://gitlab.com/ollb/timeql-grafana-plugin/-/packages)
or the [releases page](https://gitlab.com/ollb/timeql-grafana-plugin/-/releases). The contents of
the tarball need to be extracted in the Grafana plugin directory (usually `/var/lib/grafana/plugins`).

_Note_: the plugin is not signed and Grafana will therefore refuse to load it automatically. You
can override this, by setting:

```
[plugins]
allow_loading_unsigned_plugins = ollb-timeql-datasource
```

in the `/etc/grafana/grafana.ini` file.

## Configuration

The plugin only requires the URL of the TimeQL Server in the configuration. The port is usually
5000. If the TimeQL Server is running on the same machine, you can use `http://localhost:5000` as
the URL.

Please note that there is currently no authentication support. You should therefore make sure that
the TimeQL server port is closed to the outside world.

## Variables

You can use dashboard variables inside TimeQL queries using `${variable}`. Please note that this
will simply insert the text of the variable value in the query. If you need the value as a string
inside the query, you need to quote the variable as `"${variable}"`.